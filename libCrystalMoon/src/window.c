/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <GLFW/glfw3.h>

#include <init.h>
#include <window.h>
#include <version.h>

static const char *default_window_title = "CrystalMoon Window " CM_VERSION;
static const unsigned int default_window_weight = 1270,
                          default_window_height = 720;

static GLFWwindow *current_window = NULL;

static bool window_apply(cm_window *window)
{
    if (window == NULL)
        return true;

    cm_window_settings settings = window->settings;

    if (window->glfw_window == NULL)
        goto create_window;

    /* Check fullscreen */
    bool is_fullscreen =
        glfwGetWindowMonitor(window->glfw_window) == glfwGetPrimaryMonitor();

    if (is_fullscreen != settings.fullscreen)
        /* Apply fullscreen */
        goto recreate_window;

    /* Change window size */
    glfwSetWindowSize(window->glfw_window, settings.width, settings.height);
    return false;

    recreate_window:
        /* Destroy window */
        glfwDestroyWindow(window->glfw_window);

    create_window: ;
        GLFWmonitor *monitor =
            settings.fullscreen ? glfwGetPrimaryMonitor() : NULL;

        window->glfw_window = glfwCreateWindow(settings.width, settings.height,
            settings.title, monitor, NULL);

        if (window->glfw_window == NULL)
            /* Window creating failed */
            return true;

        /* Set cursor (if defined) */
        if (window->glfw_cursor != NULL)
            glfwSetCursor(window->glfw_window, window->glfw_cursor);

        return false;
}

static void window_set_context_setting(cm_window *window)
{
    /* window->glfw_window == current_window */

    /* Set default settings */
    glfwWindowHint(GLFW_RESIZABLE, false);
    glfwSwapInterval(window->settings.vsync ? 1 : 0);
}

bool window_create(cm_window *window, cm_window_settings settings)
{
    if (window == NULL)
        return true;

    if (settings.title == NULL)
        settings.title = default_window_title;

    if (settings.width == 0 || settings.height == 0) {
        settings.width = default_window_weight;
        settings.height = default_window_height;

        /* Do not break display with default resolution. */
        settings.fullscreen = false;
    }

    window->glfw_window = NULL;
    window->glfw_cursor = NULL;
    window->settings = settings;

    return window_apply(window);
}

void window_destroy(cm_window *window)
{
    if (window == NULL)
        return;

    glfwDestroyWindow(window->glfw_window);

    memset(window, 0, sizeof(cm_window));
}

void window_swap(cm_window *window)
{
    if (window)
        glfwSwapBuffers(window->glfw_window);
}

void window_poll_events(void)
{
    glfwPollEvents();
}

void window_set_settings(cm_window *window, cm_window_settings settings)
{
    if (window == NULL)
        return;

    window->settings = settings;

    window_apply(window);

    if (window->glfw_window == current_window)
        window_set_context_setting(window);
}

void window_get_settings(cm_window *window, cm_window_settings *settings)
{
    *settings = window->settings;
}

GLFWwindow *window_get_glfw_window(cm_window *window)
{
    if (window == NULL)
        return NULL;

    return window->glfw_window;
}

void window_set_current(cm_window *window)
{
    if (window == NULL)
        return;

    current_window = window->glfw_window;
    glfwMakeContextCurrent(window->glfw_window);
    window_set_context_setting(window);
}
