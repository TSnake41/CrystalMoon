/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <sprite.h>

#define STB_IMAGE_IMPLEMENTATION
/* Redefine assert */
#define STBI_ASSERT(x)
#include <stb_image.h>

bool sprite_load(cm_sprite *sprite, const char *file, bool alpha)
{
    if (sprite == NULL || file == NULL)
        return true;

    int n;

    sprite->has_alpha = alpha;
    sprite->duplicated = false;
    sprite->data =
        stbi_load(file, &sprite->width, &sprite->height, &n, alpha ? 4 : 3);

    if (sprite->data == NULL)
        return true;

    return false;
}

void sprite_unload(cm_sprite *sprite)
{
    if (sprite == NULL || sprite->data == NULL)
        return;

    if (sprite->duplicated)
        free(sprite->data);
    else
        stbi_image_free(sprite->data);
}

static void data_rgba_to_rgb(const uint8_t *rgba, uint8_t *rgb,
    unsigned int pixel_count)
{
    for (size_t i = 0; i < pixel_count; i++)
        memcpy(rgb + i * 3, rgba + i * 4, 3);
}

static void data_rgb_to_rgba(const uint8_t *rgb, uint8_t *rgba,
    unsigned int pixel_count)
{
    for (unsigned int i = 0; i < pixel_count; i++) {
        memcpy(rgba + i * 4, rgb + i * 3, 4);
        /* Alpha is the fourth component, set alpha channel to FF */
        rgba[i * 4 + 3] = 0xFF;
    }
}

bool sprite_duplicate(cm_sprite *new_sprite, cm_sprite *original, bool alpha)
{
    /* If alpha : 4 (RGBA), otherwise : 3 (RGB) */
    int byte_per_pixel = alpha ? 4 : 3;

    /* Number of pixel */
    unsigned int pixel_count = original->width * original->height;

    /* Size in bytes of original sprite data. */
    size_t buffer_size = pixel_count * byte_per_pixel;

    /* Create new block (for new_sprite). */
    void *new_block = malloc(buffer_size);
    if (new_block == NULL)
        return true;

    /* Define variables */
    new_sprite->duplicated = true;
    new_sprite->width = original->width;
    new_sprite->height = original->height;
    new_sprite->has_alpha = alpha;

    new_sprite->data = new_block;

    /* Copy or convert original sprite to new sprite. */
    if (alpha == original->has_alpha) {
        /* Just copy original to new. */
        memcpy(new_block, original->data, buffer_size);
    } else {
        /* Need to add or remove alpha component to fit with parameter. */

        /* NOTE: alpha = !original->has_alpha; */
        if (alpha)
            data_rgb_to_rgba(original->data, new_sprite->data, pixel_count);
        else
            data_rgba_to_rgb(original->data, new_sprite->data, pixel_count);
    }

    return false;
}
