/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <stdbool.h>

#include <GLFW/glfw3.h>

#include <vector.h>
#include <sprite.h>
#include <window.h>
#include <cursor.h>

bool cursor_load_custom(cm_cursor *cursor, cm_sprite sprite, cm_vector hotpoint)
{
    if (cursor == NULL || sprite.data == NULL)
        return true;

    cm_sprite cursor_sprite;
    bool duplicated = false;

    if (!sprite.has_alpha) {
        /* Duplicate it to add alpha component. */
        if (sprite_duplicate(&cursor_sprite, &sprite, true))
            return true;

        duplicated = true;
    } else
        cursor_sprite = sprite;

    GLFWimage image = {
        .width = cursor_sprite.width,
        .height = cursor_sprite.height,
        .pixels = cursor_sprite.data
    };

    *cursor = glfwCreateCursor(&image, hotpoint.x, hotpoint.y);
    if (*cursor == NULL) {
        if (duplicated)
            sprite_unload(&cursor_sprite);

        return true;
    }

    if (duplicated)
        sprite_unload(&cursor_sprite);

    return false;
}

bool cursor_load_default(cm_cursor *cursor)
{
    if (cursor == NULL)
        return true;

    /* Just create a NULL cursor. */
    *cursor = NULL;

    return false;
}

void cursor_unload(cm_cursor *cursor)
{
    if (cursor != NULL && *cursor != NULL)
        glfwDestroyCursor(*cursor);
}

void cursor_set(cm_cursor *cursor, cm_window *window)
{
    window->glfw_cursor = *cursor;

    glfwSetCursor(window->glfw_window, *cursor);
}

void cursor_toggle(cm_window window, bool visible)
{
    glfwSetInputMode(window.glfw_window, GLFW_CURSOR,
        visible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

void cursor_set_default(cm_window *window)
{
    cm_cursor cursor;
    cursor_load_default(&cursor);

    cursor_set(&cursor, window);
}
