/*
    Copyright (c) 2017 ASTIE Teddy

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>

#include <version.h>
#include <window.h>

void cm_init(void)
{
    puts("CrystalMoon " CM_VERSION "\n");

    fputs("GLFW version : ", stdout);
    puts(glfwGetVersionString());

    glfwInit();
}

void cm_terminate(void)
{
    glfwTerminate();
}

void cm_fail_safe(const char *message)
{
    fputs("Fail-safe triggered : ", stderr);
    fputs(message, stderr);

    cm_terminate();

    fputs("\nExited by fail-safe.", stderr);
    exit(1);
}
