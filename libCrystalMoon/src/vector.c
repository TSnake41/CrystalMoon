/*
    Simple 2D Vector implementation in libCrystalMoon.

    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <vector.h>

const cm_vector vector_zero = { 0, 0 };
const cm_vector vector_one = { 1, 1 };

cm_vector vector_add(cm_vector a, cm_vector b)
{
    return (cm_vector){ a.x + b.x, a.y + b.y };
}

cm_vector vector_sub(cm_vector a, cm_vector b)
{
    return (cm_vector){ a.x - b.x, a.y - b.y };
}

cm_vector vector_mul(cm_vector a, cm_vector b)
{
    return (cm_vector){ a.x * b.x, a.y * b.y };
}

cm_vector vector_div(cm_vector a, cm_vector b)
{
    return (cm_vector){ a.x / b.x, a.y / b.y };
}
