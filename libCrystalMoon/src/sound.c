/*
    Copyright (c) 2017 ASTIE Teddy

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

/* Load ogg vorbis and flac sounds (flac is better than wav for some reasons).
   Use libvorbis+libogg (or stb_vorbis) and libFLAC (or dr_flac).
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sound.h>

/* Include dr_flac */
#define DR_FLAC_IMPLEMENTATION
#include <dr_flac.h>

/* Include stb_vorbis */
#define STB_VORBIS_IMPLEMENTATION
#define STB_VORBIS_MAX_CHANNELS 8
#include <stb_vorbis.h>

bool sound_load(cm_sound *sound, const char *file)
{

}
