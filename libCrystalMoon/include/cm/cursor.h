/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef H_CM_CURSOR
#define H_CM_CURSOR

#include <GLFW/glfw3.h>

#include "vector.h"
#include "sprite.h"
#include "window.h"

typedef GLFWcursor *cm_cursor;

/**
 * Load default cursor.
 */
bool cursor_load_default(cm_cursor *cursor);

/**
 * Load cursor from a sprite.
 */
bool cursor_load_custom(cm_cursor *cursor, cm_sprite sprite, cm_vector hotpoint);

/**
 * Unload a cursor.
 */
void cursor_unload(cm_cursor *cursor);

/**
 * Set window's cursor.
 */
void cursor_set(cm_cursor *cursor, cm_window *window);

/**
 * Toggle cursor visibility.
 */
void cursor_toggle(cm_window window, bool visible);

/**
 * Reset window's cursor to default.
 */
void cursor_set_default(cm_window *window);

#endif /* H_CM_CURSOR */
