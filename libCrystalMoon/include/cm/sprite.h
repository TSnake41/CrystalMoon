/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef H_CM_SPRITE
#define H_CM_SPRITE

#include <stdbool.h>

/*
    In CrystalMoon, a sprite is just a bunch of pixel with or without an alpha
    component, and with width and height and
*/

typedef struct cm_sprite {
    int width, height;
    bool has_alpha;
    bool duplicated;

    void *data;
} cm_sprite;

/**
 * Load a sprite from file.
 */
bool sprite_load(cm_sprite *sprite, const char *file, bool alpha);

/**
 * Unload a sprite.
 */
void sprite_unload(cm_sprite *sprite);

/**
 * Duplicate a sprite, convert sprite data if needed.
 * You will manually unload new_sprite.
 */
bool sprite_duplicate(cm_sprite *new_sprite, cm_sprite *original, bool alpha);

#endif /* H_CM_SPRITE */
