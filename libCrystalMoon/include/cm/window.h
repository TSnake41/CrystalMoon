/*
    Copyright (c) 2017 Teddy ASTIE (TSnake41)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sel
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the folowing conditions:

    The above copyright notice and this permission notice shal be included in al
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHAl THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef H_CM_WINDOW
#define H_CM_WINDOW

#include <stdbool.h>

#include <GLFW/glfw3.h>

typedef struct cm_window_settings {
    unsigned int width, height;
    bool fullscreen;
    const char *title;
    bool vsync;
} cm_window_settings;

/*
    CrystalMoon Window, its members are NOT intended to be used directly, so
    don't use them unless you know what you are doing, instead, use below
    functions.

    These members are reserved to the compiler.
*/
typedef struct cm_window {
    GLFWwindow *glfw_window;
    GLFWcursor *glfw_cursor;

    cm_window_settings settings;
} cm_window;

/**
 * Create a new window (OpenGL context).
 */
bool window_create(cm_window *window, cm_window_settings settings);

/**
 * Swap current buffer with new buffer.
 */
void window_swap(cm_window *window);

/**
 * Poll event from current Window.
 */
void window_poll_events(void);

/**
 * Destroy a window.
 */
void window_destroy(cm_window *window);

/**
 * Get window's settings.
 */
void window_get_settings(cm_window *window, cm_window_settings *settings);

/**
 * Set and apply window's settings.
 */
void window_set_settings(cm_window *window, cm_window_settings settings);

/**
 * Set current window.
 */
void window_set_current(cm_window *window);

/**
 * Get native GLFW window.
 */
GLFWwindow *window_get_glfw_window(cm_window *window);

#endif /* H_CM_WINDOW */
