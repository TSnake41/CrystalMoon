#include <stdio.h>
#include <stdbool.h>

#include <tiny_assert.h>

#include <cm/init.h>
#include <cm/window.h>
#include <cm/cursor.h>
#include <cm/sthread.h>

int main(void)
{
    cm_init();

    cm_window window;
    cm_window_settings settings = {
        .title = NULL,
        .fullscreen = false,
        .width = 1024,
        .height = 768,
        .vsync = true
    };

    cm_cursor cursor;
    cm_sprite cursor_sprite;

    sprite_load(&cursor_sprite, "cursor.png", true);

    window_create(&window, settings);
    cursor_load_custom(&cursor, cursor_sprite, (cm_vector){ 0.0f, 0.0f });

    cursor_set(&cursor, &window);

    window_set_current(&window);

    while (!glfwWindowShouldClose(window.glfw_window)) {
        glClearColor(.2f, .5f, 1.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);

        window_swap(&window);
        window_poll_events();
    }

    window_destroy(&window);
    sprite_unload(&cursor_sprite);

    cm_terminate();
    return 0;
}
